USE clinicalTrials;

DELIMITER //
drop procedure if exists addEquipamento;
create procedure addEquipamento(in designacaoN varchar(45),in reutilizavelN varchar(1))
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Equipamento VALUES (null,designacaoN,reutilizavelN);
    COMMIT;
end//
DELIMITER ;