USE clinicalTrials;

DELIMITER //
drop procedure if exists addAtleta;
create procedure addAtleta(in emailN varchar(45),in nomeN varchar(45),in numT varchar(45),in dataN date,in alturaN int,in pesoN int,in idModalidade int,in generoN varchar(1))
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Atleta VALUES (null,emailN,nomeN,numT,dataN,pesoN,idModalidade,generoN);
    COMMIT;
end//
DELIMITER ;