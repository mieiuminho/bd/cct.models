USE clinicalTrials;

DELIMITER //
drop procedure if exists getConsultasPorEspecialidade;
CREATE PROCEDURE getConsultasPorEspecialidade(in atletaID int)
BEGIN
    select distinct e.nome as 'especialidade', count(c.id) as "numero de consultas"
    from	
      (select * from Consulta where Consulta.idAtleta=atletaID) as c
      join Medico as m on c.idMedico = m.id join Especialidade as e on e.id = m.especialidade_id
	group by e.nome;
END //
 
DELIMITER ;

