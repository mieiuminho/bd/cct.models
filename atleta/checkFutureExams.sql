DELIMITER //
drop procedure if exists checkFutureExams;
create procedure checkFutureExams(in ida int)
begin
     select e.designacao as Exame,c.hora_inicio as "Data" from Consulta as c inner join (select * from Atleta where Atleta.id=ida) as a 
																			 inner join Exame_In_Consulta as eic on eic.consulta_id=c.id
                                                                             inner join Exame as e on eic.exame_id=e.id
														  where c.hora_inicio > curdate();
end//
DELIMITER ;

