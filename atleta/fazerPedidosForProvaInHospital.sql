USE clinicalTrials;

DELIMITER $$
drop procedure if exists fazerPedidosForProvaInHospital;

CREATE PROCEDURE fazerPedidosForProvaInHospital(
    IN provaID int,in atletaID int,in hospitalID int
)
BEGIN
    DECLARE finished INTEGER DEFAULT 0;
	declare exameN integer;
    declare datalim datetime;
    -- declare cursor for exams needed to enter
    
    DEClARE curExamesNeeded 
        CURSOR FOR 
            SELECT ep.idExame,ep.data_limite FROM ExamesNeededInProva as ep join Prova as p on ep.idProva=p.id;
 
    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
 
    OPEN curExamesNeeded;
 
    getExams: LOOP
        FETCH curExamesNeeded INTO exameN,datalim;
        IF finished = 1 THEN 
            LEAVE getExams;
        END IF;
        if exists (select m.id from Consulta as c right join Medico as m on c.idMedico=m.id
					where m.especialidade_id=(select distinct e.idEspecialidade from Exame as e where e.id=exameN) and m.hospital=hospitalID)
			then begin
					if not exists (select * from Pedidos_Exame as pe where pe.idExame = exameN) then
                    begin
						if not exists (select * from 
						(select * from Consulta where Consulta.idAtleta=atletaID) as c
						join Exame_In_Consulta as ec on ec.consulta_id=c.id where ec.exame_id=exameN and datalim <= c.hora_fim) then
						begin
							insert into Pedidos_Exame values(atletaId,hospitalID,exameN);
						end; 
						end if;
					end;
					end if;
				end;
			else begin
					ROLLBACK;
                    select "Não foi possível marcar todos os exames pois não há médicos suficientes neste hospital!";
                    set finished = 1;
				 end; end if;
    END LOOP getExams;
    CLOSE curExamesNeeded;
    select "Os Exames foram pedidos com sucesso!";
 
END$$
DELIMITER ;