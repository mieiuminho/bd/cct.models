USE clinicalTrials;

DELIMITER //
drop procedure if exists getExamsNeeded;
CREATE PROCEDURE getExamsNeeded(In provaID INT)
BEGIN
    Select e.designacao
    from
		Exame as e,
        ExamesNeededInProva as ep
	where 
		ep.idProva = provaID and e.id=ep.idExame;
END //
 
DELIMITER ;