USE clinicalTrials;

DELIMITER //
drop procedure if exists getFuturasProvas;
CREATE PROCEDURE getFuturasProvas(in atletaID int)
BEGIN
    select p.nome as 'prova', p.id as 'id'
    from	
      (select * from Atleta where Atleta.id = atletaID) as a join Prova as p
      on a.modalidade_id=p.idModalidade
	where p.data_inicio > curdate();
END //
 
DELIMITER ;