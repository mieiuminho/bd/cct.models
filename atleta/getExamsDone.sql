USE clinicalTrials;

DELIMITER //
drop procedure if exists getExamsDone;
CREATE PROCEDURE getExamsDone(In di DATETIME,In df DATETIME,In atletaID int)
BEGIN
    Select distinct e.designacao
    from
		Atleta as a join Consulta as c on a.id=c.idAtleta
        join Exame_In_Consulta as ec on c.id=ec.consulta_id
        join Exame as e on ec.exame_id=e.id
	where 
		di <= c.hora_inicio and df >= c.hora_fim and a.id=atletaID;
END //
DELIMITER ;

