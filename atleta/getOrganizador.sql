USE clinicalTrials;

DELIMITER //
drop procedure if exists getOrganizador;
CREATE PROCEDURE getOrganizador(In provaID INT)
BEGIN
    select o.nome 
    from
		Organizador as o, Prova as p
	where 
		p.id=provaID and o.id=p.idOrganizador;
END //
 
DELIMITER ;