USE clinicalTrials;

(SELECT 'id', 'email', 'nome', 'telemovel', 'data_nascimento', 'altura', 'peso', 'modalidade_id', 'genero')
UNION
(SELECT *
 FROM Atleta)
INTO OUTFILE 'Atleta.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'idAtleta', 'idProva')
UNION
(SELECT *
 FROM Atleta_In_Prova )
INTO OUTFILE 'Atleta_In_Prova.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'idMedico', 'idAtleta', 'hora_inicio', 'hora_fim')
UNION
(SELECT *
 FROM Consulta)
INTO OUTFILE 'Consulta.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'designacao', 'reutilizavel')
UNION
(SELECT *
 FROM Equipamento)
INTO OUTFILE 'Equipamento.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'idEquipamento', 'idExame')
UNION
(SELECT *
 FROM Equipamento_In_Exame)
INTO OUTFILE 'Equipamento_In_Exame.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'idHospital', 'idEquipamento', 'stock')
UNION
(SELECT *
 FROM Equipamento_In_Hospital)
INTO OUTFILE 'Equipamento_In_Hospital.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'nome')
UNION
(SELECT *
 FROM Especialidade)
INTO OUTFILE 'Especialidade.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'designacao', 'idEspecialidade')
UNION
(SELECT *
 FROM Exame)
INTO OUTFILE 'Exame.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'exame_id', 'consulta_id')
UNION
(SELECT *
 FROM Exame_In_Consulta)
INTO OUTFILE 'Exame_In_Consulta.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'idProva', 'idExame', 'data_limite')
UNION
(SELECT *
 FROM ExamesNeededInProva)
INTO OUTFILE 'ExamesNeededInProva.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'nome')
UNION
(SELECT *
 FROM Hospital)
INTO OUTFILE 'Hospital.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'email', 'nome', 'especialidade_id', 'hospital')
UNION
(SELECT *
 FROM Medico)
INTO OUTFILE 'Medico.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'nome')
UNION
(SELECT *
 FROM Modalidade )
INTO OUTFILE 'Modalidade.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'nome', 'email')
UNION
(SELECT *
 FROM Organizador)
INTO OUTFILE 'Organizador.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'idAtleta', 'idHospital', 'idExame')
UNION
(SELECT *
 FROM Pedidos_Exame)
INTO OUTFILE 'Pedidos_Exame.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';

(SELECT 'id', 'nome', 'idOrganizador', 'data_inicio', 'data_fim', 'idModalidade')
UNION
(SELECT *
 FROM Prova)
INTO OUTFILE 'Prova.csv'
    FIELDS ENCLOSED BY '"'
    TERMINATED BY ','
    ESCAPED BY '"'
    LINES TERMINATED BY '\n';
