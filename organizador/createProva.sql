USE clinicalTrials;

DELIMITER //
drop procedure if exists createProva;
create procedure createProva(in nomeN varchar(45),in ido int,in di datetime,in df datetime,in idm int)
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Prova VALUES (null,nomeN,ido,di,df,idm);
    COMMIT;
end//
DELIMITER ;