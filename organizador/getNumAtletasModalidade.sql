USE clinicalTrials;

DELIMITER //
drop procedure if exists getNumAtletasModalidade;
create procedure getNumAtletasModalidade(in idModalidade int)
begin
select distinct count(id) as numAtletas from Atleta as a where a.modalidade_id=idModalidade;
end//
DELIMITER ;