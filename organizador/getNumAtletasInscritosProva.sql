USE clinicalTrials;

DELIMITER //
drop procedure if exists getNumAtletasInscritosProva;
create procedure getNumAtletasInscritosProva(in idProva int)
begin
select distinct count(idAtleta) as numAtletas from Atleta_In_Prova as ap where ap.idProva=idProva;
end//
DELIMITER ;