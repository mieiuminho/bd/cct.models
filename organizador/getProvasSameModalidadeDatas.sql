USE clinicalTrials;

DELIMITER //
drop procedure if exists getProvasSameModalidadeDatas;
create procedure getProvasSameModalidadeDatas(in modalidade_id int)
begin
select nome as Prova,data_inicio as Inicio,data_fim as Fim from Prova as p where modalidade_id=p.idModalidade;
end//
DELIMITER ;