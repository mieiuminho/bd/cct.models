-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema clinicalTrials
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `clinicalTrials` ;

-- -----------------------------------------------------
-- Schema clinicalTrials
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clinicalTrials` ;
USE `clinicalTrials` ;

-- -----------------------------------------------------
-- Table `clinicalTrials`.`Especialidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Especialidade` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Especialidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(128) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Hospital`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Hospital` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Hospital` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Medico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Medico` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Medico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(128) NULL,
  `nome` VARCHAR(128) NULL,
  `especialidade_id` INT NOT NULL,
  `hospital` INT NOT NULL,
  PRIMARY KEY (`id`, `hospital`),
  CONSTRAINT `fk_Médico_Especialidade1`
    FOREIGN KEY (`especialidade_id`)
    REFERENCES `clinicalTrials`.`Especialidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Médico_1`
    FOREIGN KEY (`hospital`)
    REFERENCES `clinicalTrials`.`Hospital` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Médico_Especialidade1_idx` ON `clinicalTrials`.`Medico` (`especialidade_id` ASC) VISIBLE;

CREATE INDEX `fk_Médico_1_idx` ON `clinicalTrials`.`Medico` (`hospital` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Modalidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Modalidade` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Modalidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Atleta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Atleta` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Atleta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(128) NULL,
  `nome` VARCHAR(128) NOT NULL,
  `telemovel` VARCHAR(9) NULL,
  `data_nascimento` DATE NOT NULL,
  `altura` INT NOT NULL,
  `peso` INT NOT NULL,
  `modalidade_id` INT NOT NULL,
  `genero` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Atleta_Modalidade1`
    FOREIGN KEY (`modalidade_id`)
    REFERENCES `clinicalTrials`.`Modalidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Atleta_Modalidade1_idx` ON `clinicalTrials`.`Atleta` (`modalidade_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Consulta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Consulta` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Consulta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idMedico` INT NOT NULL,
  `idAtleta` INT NOT NULL,
  `hora_inicio` DATETIME NULL,
  `hora_fim` DATETIME NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Médico_has_Atleta_Médico`
    FOREIGN KEY (`idMedico`)
    REFERENCES `clinicalTrials`.`Medico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Médico_has_Atleta_Atleta1`
    FOREIGN KEY (`idAtleta`)
    REFERENCES `clinicalTrials`.`Atleta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Médico_has_Atleta_Atleta1_idx` ON `clinicalTrials`.`Consulta` (`idAtleta` ASC) VISIBLE;

CREATE INDEX `fk_Médico_has_Atleta_Médico_idx` ON `clinicalTrials`.`Consulta` (`idMedico` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Organizador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Organizador` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Organizador` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(128) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Prova`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Prova` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Prova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(128) NOT NULL,
  `idOrganizador` INT NOT NULL,
  `data_inicio` DATETIME NULL,
  `data_fim` DATETIME NULL,
  `idModalidade` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `email_organizador`
    FOREIGN KEY (`idOrganizador`)
    REFERENCES `clinicalTrials`.`Organizador` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idModalidade`
    FOREIGN KEY (`idModalidade`)
    REFERENCES `clinicalTrials`.`Modalidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `email_organizador_idx` ON `clinicalTrials`.`Prova` (`idOrganizador` ASC) VISIBLE;

CREATE INDEX `idModalidade_idx` ON `clinicalTrials`.`Prova` (`idModalidade` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Atleta_In_Prova`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Atleta_In_Prova` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Atleta_In_Prova` (
  `idAtleta` INT NOT NULL,
  `idProva` INT NOT NULL,
  PRIMARY KEY (`idAtleta`, `idProva`),
  CONSTRAINT `Atleta_email`
    FOREIGN KEY (`idAtleta`)
    REFERENCES `clinicalTrials`.`Atleta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idProva`
    FOREIGN KEY (`idProva`)
    REFERENCES `clinicalTrials`.`Prova` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `Prova_nome_idx` ON `clinicalTrials`.`Atleta_In_Prova` (`idProva` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Exame`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Exame` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Exame` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `designacao` VARCHAR(48) NOT NULL,
  `idEspecialidade` INT NOT NULL,
  PRIMARY KEY (`id`, `idEspecialidade`),
  CONSTRAINT `fk_idEspecialidade`
    FOREIGN KEY (`idEspecialidade`)
    REFERENCES `clinicalTrials`.`Especialidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_idEspecialidade_idx` ON `clinicalTrials`.`Exame` (`idEspecialidade` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Exame_In_Consulta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Exame_In_Consulta` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Exame_In_Consulta` (
  `exame_id` INT NOT NULL,
  `consulta_id` INT NOT NULL,
  PRIMARY KEY (`exame_id`, `consulta_id`),
  CONSTRAINT `exame_id`
    FOREIGN KEY (`exame_id`)
    REFERENCES `clinicalTrials`.`Exame` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `consulta_id`
    FOREIGN KEY (`consulta_id`)
    REFERENCES `clinicalTrials`.`Consulta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `consulta_id_idx` ON `clinicalTrials`.`Exame_In_Consulta` (`consulta_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Equipamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Equipamento` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Equipamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `designacao` VARCHAR(45) NOT NULL,
  `reutilizavel` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Equipamento_In_Exame`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Equipamento_In_Exame` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Equipamento_In_Exame` (
  `idEquipamento` INT NOT NULL,
  `idExame` INT NOT NULL,
  PRIMARY KEY (`idEquipamento`, `idExame`),
  CONSTRAINT `fk_Equipamento_In_Exame_1`
    FOREIGN KEY (`idExame`)
    REFERENCES `clinicalTrials`.`Exame` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Equipamento_In_Exame_2`
    FOREIGN KEY (`idEquipamento`)
    REFERENCES `clinicalTrials`.`Equipamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Equipamento_In_Exame_1_idx` ON `clinicalTrials`.`Equipamento_In_Exame` (`idExame` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Equipamento_In_Hospital`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Equipamento_In_Hospital` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Equipamento_In_Hospital` (
  `idHospital` INT NOT NULL,
  `idEquipamento` INT NOT NULL,
  `stock` INT NOT NULL,
  PRIMARY KEY (`idHospital`, `idEquipamento`),
  CONSTRAINT `fk_Equipamento_In_Hospital_1`
    FOREIGN KEY (`idEquipamento`)
    REFERENCES `clinicalTrials`.`Equipamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Equipamento_In_Hospital_2`
    FOREIGN KEY (`idHospital`)
    REFERENCES `clinicalTrials`.`Hospital` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Equipamento_In_Hospital_1_idx` ON `clinicalTrials`.`Equipamento_In_Hospital` (`idEquipamento` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`ExamesNeededInProva`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`ExamesNeededInProva` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`ExamesNeededInProva` (
  `idProva` INT NOT NULL,
  `idExame` INT NOT NULL,
  `data_limite` DATETIME NULL,
  PRIMARY KEY (`idProva`, `idExame`),
  CONSTRAINT `fk_ExamesNeededInProva_1`
    FOREIGN KEY (`idProva`)
    REFERENCES `clinicalTrials`.`Prova` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ExamesNeededInProva_2`
    FOREIGN KEY (`idExame`)
    REFERENCES `clinicalTrials`.`Exame` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_ExamesNeededInProva_2_idx` ON `clinicalTrials`.`ExamesNeededInProva` (`idExame` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `clinicalTrials`.`Pedidos_Exame`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clinicalTrials`.`Pedidos_Exame` ;

CREATE TABLE IF NOT EXISTS `clinicalTrials`.`Pedidos_Exame` (
  `idAtleta` INT NOT NULL,
  `idHospital` INT NOT NULL,
  `idExame` INT NOT NULL,
  PRIMARY KEY (`idAtleta`, `idHospital`, `idExame`),
  CONSTRAINT `idAtleta_fk`
    FOREIGN KEY (`idAtleta`)
    REFERENCES `clinicalTrials`.`Atleta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idHospital_fk`
    FOREIGN KEY (`idHospital`)
    REFERENCES `clinicalTrials`.`Hospital` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idExame_fk`
    FOREIGN KEY (`idExame`)
    REFERENCES `clinicalTrials`.`Exame` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `idHospital_fk_idx` ON `clinicalTrials`.`Pedidos_Exame` (`idHospital` ASC) VISIBLE;

CREATE INDEX `idExame_fk_idx` ON `clinicalTrials`.`Pedidos_Exame` (`idExame` ASC) VISIBLE;

USE `clinicalTrials` ;

-- -----------------------------------------------------
-- View `clinicalTrials`.`viewProvasOrganizador`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `clinicalTrials`.`viewProvasOrganizador` ;
USE `clinicalTrials`;
CREATE  OR REPLACE VIEW `viewProvasOrganizador` AS
    SELECT 
        o.id AS idOrganizador,
        o.nome AS nomeOrganizador,
        p.nome AS nomeProva,
        m.nome AS nomeModalidade,
        COUNT(DISTINCT ap.idAtleta) as numAtletas
    FROM
        Organizador AS o
            INNER JOIN
        Prova AS p ON o.id = p.idOrganizador
            INNER JOIN
        Modalidade AS m ON p.idModalidade = m.id
            INNER JOIN
        Atleta_In_Prova AS ap ON ap.idProva = p.id
    GROUP BY p.id;

-- -----------------------------------------------------
-- View `clinicalTrials`.`viewExamesProvasAtleta`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `clinicalTrials`.`viewExamesProvasAtleta` ;
USE `clinicalTrials`;
CREATE  OR REPLACE VIEW `viewExamesProvasAtleta` AS
    SELECT 
        p.nome AS nomeProva,
        m.nome AS modalidade,
        e.designacao AS exame,
        esp.nome AS especialidade,
        ep.data_limite AS dataLimite
    FROM
        Modalidade AS m
            INNER JOIN
        Prova AS p ON m.id = p.idModalidade
            INNER JOIN
        ExamesNeededInProva AS ep ON p.id = ep.idProva
            INNER JOIN
        Exame AS e ON ep.idExame = e.id
            INNER JOIN
        Especialidade AS esp ON e.idEspecialidade = esp.id
    WHERE
        p.data_fim > CURDATE();

-- -----------------------------------------------------
-- View `clinicalTrials`.`viewHospitais`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `clinicalTrials`.`viewHospitais` ;
USE `clinicalTrials`;
CREATE  OR REPLACE VIEW `viewHospitais` AS
    SELECT 
        m.id AS idMedico,
        m.nome AS nomeMedico,
        h.nome AS nomeHospital,
        COUNT(DISTINCT c.id) AS numConsultas
    FROM
        Hospital AS h
            INNER JOIN
        Medico AS m ON h.id = m.hospital
            INNER JOIN
        Consulta AS c ON m.id = c.idMedico
    GROUP BY m.id;

-- -----------------------------------------------------
-- View `clinicalTrials`.`viewPedidosMedico`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `clinicalTrials`.`viewPedidosMedico` ;
USE `clinicalTrials`;
CREATE  OR REPLACE VIEW `viewPedidosMedico` AS
    SELECT 
        a.id AS atletaID,
        a.nome AS atletaNome,
        h.id AS hospitalID,
        h.nome AS hospitalNome,
        e.designacao AS exame
    FROM
        Pedidos_Exame AS pe
            INNER JOIN
        Atleta AS a ON pe.idAtleta = a.id
            INNER JOIN
        Hospital AS h ON pe.idHospital = h.id
            INNER JOIN
        Exame AS e ON pe.idExame = e.id;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
