USE clinicalTrials;

DELIMITER //
drop procedure if exists hireDoc;
create procedure hireDoc(in emailN varchar(45),in nomeN varchar(45),in ide int,in idh int)
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Medico VALUES (null,emailN,nomeN,ide,idh);
    COMMIT;
end//
DELIMITER ;