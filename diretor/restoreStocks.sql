USE clinicalTrials;

DELIMITER //
drop procedure if exists restoreStocks;
create procedure restoreStocks(in idh int,in ide int,in difs int)
begin
	 declare exit handler for sqlexception rollback;
     start transaction;
     update Equipamento_In_Hospital as eh set stock = stock + difs where eh.idHospital=idH and eh.idEquipamento=ide;
     commit;
end//
DELIMITER ;