USE clinicalTrials;

DELIMITER //
drop procedure if exists checkStocks;
create procedure checkStocks(in idh int)
begin
     select eq.designacao as Equipamento, eh.stock as Stock from (select * from Equipamento_In_Hospital where idHospital=idh) as eh
															join Equipamento as eq on eh.idEquipamento=eq.id;
end//
DELIMITER ;