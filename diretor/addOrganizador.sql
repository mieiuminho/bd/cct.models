USE clinicalTrials;

DELIMITER //
drop procedure if exists addOrganizador;
create procedure addOrganizador(in nomeN varchar(45),in emailN varchar(45))
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Exame VALUES (null,nomeN,emailN);
    COMMIT;
end//
DELIMITER ;