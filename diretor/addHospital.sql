USE clinicalTrials;

DELIMITER //
drop procedure if exists addHospital;
create procedure addHospital(in nomeN varchar(45))
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Hospital VALUES (null,nomeN);
    COMMIT;
end//
DELIMITER ;