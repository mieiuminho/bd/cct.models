USE clinicalTrials;

DELIMITER //
drop procedure if exists addExame;
create procedure addExame(in designacaoN varchar(45),in ien int)
begin
	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK; 
    START TRANSACTION;
        INSERT INTO Exame VALUES (null,designacaoN,ien);
    COMMIT;
end//
DELIMITER ;