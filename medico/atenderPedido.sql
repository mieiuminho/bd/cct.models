USE clinicalTrials;

DELIMITER //
drop procedure if exists atenderPedido;
create procedure atenderPedido(in idm int,in ida int,in ide int,in di datetime,in df datetime)
begin
	 declare idh int;
	 declare idc int;
     declare exit handler for sqlexception rollback;
     start transaction;
	 set idh = (select hospital from Medico where id=idm limit 1);
     delete from Pedidos_Exame as pe where pe.idAtleta=ida and pe.idExame=ide and pe.idHospital=idh;
     insert into Consulta values (null,idm,ida,di,df);
     set idc = (select max(id) from Consulta);
     insert into Exame_In_Consulta values (ide,idc);
     commit;
end//
DELIMITER ;