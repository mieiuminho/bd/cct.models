USE clinicalTrials;

DELIMITER //
drop procedure if exists atenderPedidoToConsulta;
create procedure atenderPedidoToConsulta(in idm int,in ida int,in ide int,in idc int)
begin
	 declare idh int;
     declare exit handler for sqlexception rollback;
     start transaction;
	 set idh = (select hospital from Medico where id=idm limit 1);
     delete from Pedidos_Exame as pe where pe.idAtleta=ida and pe.idExame=ide and pe.idHospital=idh;
     if exists (select * from Consulta as c where c.id=idc and c.idMedico=idm and c.idAtleta=ida) and exists
     (select * from (select * from Exame where id=ide) as e join Medico as m on e.idEspecialidade=m.especialidade_id and m.id=idm)then
     insert into Exame_In_Consulta values (ide,idc);
     else rollback; end if;
     commit;
end//
DELIMITER ;